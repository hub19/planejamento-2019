# Hub19 - Planejamento 2019

O Hub19 é um grupo de profissionais empreendedores que visam fomentar o ecossistema de Campinas e Região e, por ser um grupo relativamente novo, é importante definir seu planejamento para 2019 em conjunto com sua missão e escopo de atuação.

Para isto, este projeto público visa auxiliar na gestão de datas e agenda da realização do planejamento e, posteriormente, de sua execução.

Caso você queira saber mais sobre este projeto, sinta-se a vontade para entrar em contato com @jboilesen.
